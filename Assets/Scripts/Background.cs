﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Background : MonoBehaviour {
	private Material mat;
	public float speed;
	private Vector2 offset;
	// Use this for initialization
	void Awake () {
		mat = GetComponent <Renderer> ().material;
		offset = new Vector2 (0, 0);
	}
	
	// Update is called once per frame
	void Update () {
		offset.y += speed * Time.deltaTime;
		if (offset.y > 100) {
			offset.y -= 100;
		}
		mat.SetTextureOffset ("_MainTex", offset);
	}
}
